#ifndef TRIMESH_VIEWER_MAINWINDOW_INCLUDED
#define TRIMESH_VIEWER_MAINWINDOW_INCLUDED

// #include <trimesh.h>

#include <QMainWindow>
#include <QAbstractItemModel>
#include <QModelIndex>
#include <QVariant>
#include <QItemSelectionModel>
#include <QTreeView>
#include <QSortFilterProxyModel>
#include <QTimer>

#include <PythonQt.h>
#include <PythonQtScriptingConsole.h>
#include <QGLViewer/qglviewer.h>

#include <boost/any.hpp>

namespace glutils
{

class renderable_t;

typedef boost::shared_ptr<renderable_t> renderable_ptr_t;

class glviewer_t : public QGLViewer
{
  Q_OBJECT

  std::vector<renderable_ptr_t> m_rens;

public:
  bool m_is_recording;
  bool m_bf_cull;
  bool m_wireframe;

  glviewer_t(QWidget *par);

  ~glviewer_t();

public:
  void add_ren(renderable_ptr_t ren);
  void del_ren(int i);


protected:

  virtual void draw();
  virtual void init();
  virtual void animate();
  virtual QString helpString() const;
  virtual void keyPressEvent(QKeyEvent *e);

public:
  bool saveImageSnapshot(const QString& ,int );

  bool setLightParams(int ind, QVariantMap params);

public slots:
  bool setLightEnabled(int ind, bool v);
  bool setLightPosition(int ind, double x,double y,double z,double w);


};

}

#include <ui_mainwindow.h>

namespace glutils
{

class viewer_mainwindow:
    public QMainWindow,
    public Ui::MainWindow
{

  Q_OBJECT

public:


  PythonQtObjectPtr          m_pqt;
public:

  viewer_mainwindow();

  ~viewer_mainwindow();

private slots:
  void on_actionEval_Script_triggered(bool);

public slots:

  void eval_script(QString str);

  void save_snapshot(QString str,int ms);

};


}

//#include <QGraphicsItem>
//#include <QPainter>
//namespace spin
//{
//  inline QPointF to_qpointf(const si_point_t &p)
//  {
//    return QPointF(p[0],p[1]);
//  }

//  inline QPoint to_qpoint(const si_ipoint_t &p)
//  {
//    return QPoint(p[0],p[1]);
//  }

//  class si_graphics_item_t : public QGraphicsItem
//  {
//  private:
//    trimesh::viewer_t * m_viewer;

//    QImage             *m_image;
//    spin_image_ptr_t    m_si;

//  public:

//    si_graphics_item_t(trimesh::viewer_t* v):m_viewer(v){m_image = NULL;};

//    void update_image();

//    QRectF boundingRect() const;

//    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
//               QWidget *widget);
//  };

//  typedef boost::shared_ptr<si_graphics_item_t> si_graphics_item_ptr_t;
//}


#endif
