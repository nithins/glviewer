#include <QMenu>
#include <QTreeView>
#include <QColorDialog>
#include <QDebug>
#include <QKeyEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QDir>

#include <boost/typeof/typeof.hpp>
#include <boost/range/algorithm.hpp>

#include <mainwindow.h>
#include <glutils.h>

#include <QJsonArray>
#include <QJsonDocument>
#include <QList>
#include <QVector4D>

using namespace std;


// two directional lights by default
// as a convention directional lights are directed along 0,0,1 model coords
// and positional lights are located at 0,0,0 model coords
// another frame is associated with the light to move it around
// and orient it in world coords
glutils::light_properties_t g_lights_default[] =
{
  {
    la::make_vec<float>(0.2f, 0.2f, 0.2f, 1.0f), // ambient
    la::make_vec<float>(0.5f, 0.5f, 0.5f, 1.0f), // diffuse
    la::make_vec<float>(0.7f, 0.7f, 0.7f, 0.7f), // specular
    la::make_vec<float>(5.0f, 5.0f, 0.0f, 0.0f), // position
    1,0.5,0.5,                    // attenution c + l + q
    true                      // enabled
  },

  {
    la::make_vec<float>(0.2f, 0.2f, 0.2f, 1.0f), // ambient
    la::make_vec<float>(0.5f, 0.5f, 0.5f, 1.0f), // diffuse
    la::make_vec<float>(0.7f, 0.7f, 0.7f, 0.7f), // specular
    la::make_vec<float>(5.0f, -5.0f, 0.0f, 0.0f), // position
    1,0.5,0.5,                    // attenution c + l + q
    true                      // enabled
  }
};

glutils::material_properties_t g_surface_material_default =
{
  la::make_vec<float>(0.2,0.2,0.2,1),// ambient
  la::make_vec<float>(0.5,0.5,0.5,1),// diffuse
  la::make_vec<float>(0.8,0.8,0.8,1),      // specular
  la::make_vec<float>(0,0,0,1),      // emission
  66             // shininess
};

namespace glutils
{

void glviewer_t::draw()
{
  glPushAttrib(GL_ENABLE_BIT);

  glEnable(GL_RESCALE_NORMAL);

  glPushMatrix();

  GLfloat lmodel_ambient[] = { 0.0, 0.0, 0.0, 1.0 };
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, lmodel_ambient);

  g_lights_default[0].render(0);
  g_lights_default[1].render(1);
  g_surface_material_default.render_all(GL_FRONT_FACE);


  glEnable(GL_COLOR_MATERIAL);
  glColorMaterial(GL_FRONT,GL_AMBIENT_AND_DIFFUSE);

  glPopMatrix();

  glPushMatrix();

  glScalef(0.1,0.1,0.1);

  for(int i = 0 ; i < m_rens.size(); ++i)
    m_rens[i]->render();

  glPopMatrix();

  glPopAttrib();
}



bool glviewer_t::setLightParams(int ind, QVariantMap map)
{
  if(!is_in_range(ind,0,2))
  {
    WLOG << "invalid index = " << ind ;
    return false;
  }

  if(map.contains("Enabled"))
  {
    g_lights_default[ind].enabled = map["Enabled"].toBool();
  }

  if(map.contains("Position"))
  {
    QVector4D pos = map["Position"].value<QVector4D>();

    g_lights_default[ind].position = {pos[0],pos[1],pos[2],pos[3]};
  }

  return true;
}


bool glviewer_t::saveImageSnapshot(const QString& fileName,int over_sample)
{
  double oversampling = over_sample;
  QSize subSize(int(this->width()/oversampling),
                int(this->height()/oversampling));

  QSize finalSize = this->size();

  double aspectRatio = width() / static_cast<double>(height());

  double zNear = camera()->zNear();
  double zFar = camera()->zFar();

  double xMin, yMin;
  if (camera()->type() == qglviewer::Camera::PERSPECTIVE)
  {
    xMin = zNear * tan(camera()->fieldOfView() / 2.0) * aspectRatio;
    yMin = xMin / aspectRatio;
  }
  else
  {
    camera()->getOrthoWidthHeight(xMin, yMin);
    yMin = xMin / aspectRatio;
  }

#if QT_VERSION >= 0x040000
  QImage image(finalSize.width(), finalSize.height(), QImage::Format_ARGB32);
#else
  QImage image(finalSize.width(), finalSize.height(), 32);
#endif

  if (image.isNull())
  {
    QMessageBox::warning(this, "Image saving error",
                         "Unable to create resulting image",
                         QMessageBox::Ok, QMessageBox::NoButton);
    return false;
  }

  double deltaX = 2.0 * xMin * subSize.width() / finalSize.width();
  double deltaY = 2.0 * yMin * subSize.height() / finalSize.height();

  int nbX = finalSize.width() / subSize.width();
  int nbY = finalSize.height() / subSize.height();

  // Extra subimage on the border if needed
  if (nbX * subSize.width() < finalSize.width())
    nbX++;
  if (nbY * subSize.height() < finalSize.height())
    nbY++;

  makeCurrent();

  int count=0;
  for (int i=0; i<nbX; i++)
    for (int j=0; j<nbY; j++)
    {
      preDraw();
      // Change projection matrix
      glMatrixMode(GL_PROJECTION);
      glLoadIdentity();
      if (camera()->type() == qglviewer::Camera::PERSPECTIVE)
        glFrustum(-xMin + i*deltaX, -xMin + (i+1)*deltaX,
                  yMin - (j+1)*deltaY, yMin - j*deltaY,
                  zNear, zFar);
      else
        glOrtho(-xMin + i*deltaX, -xMin + (i+1)*deltaX,
                yMin - (j+1)*deltaY, yMin - j*deltaY,
                zNear, zFar);
      glMatrixMode(GL_MODELVIEW);

      draw();
      postDraw();


      QImage snapshot = grabFrameBuffer(true);


#if QT_VERSION >= 0x040000
      QImage subImage = snapshot.scaled(subSize, Qt::IgnoreAspectRatio,
                                        Qt::SmoothTransformation);
#else
# if QT_VERSION >= 0x030000
      QImage subImage = snapshot.scale(subSize, QImage::ScaleFree);
# else
      QImage subImage = snapshot.smoothScale(subSize.width(),
                                             subSize.height());
# endif
#endif

      // Copy subImage in image
      for (int ii=0; ii<subSize.width(); ii++)
      {
        int fi = i*subSize.width() + ii;
        if (fi == image.width())
          break;
        for (int jj=0; jj<subSize.height(); jj++)
        {
          int fj = j*subSize.height() + jj;
          if (fj == image.height())
            break;
          image.setPixel(fi, fj, subImage.pixel(ii,jj));
        }
      }
      count++;
    }

#if QT_VERSION >= 0x040000
  bool saveOK = image.save(fileName, snapshotFormat().toLatin1().constData(),
                           snapshotQuality());
#else
  bool saveOK = image.save(fileName, snapshotFormat(), snapshotQuality());
#endif


  return saveOK;
}


void glviewer_t::init()
{
  glutils::init();

  restoreStateFromFile();

  setSnapshotFormat("PNG");

  setSnapshotQuality(100);

  glEnable ( GL_CULL_FACE );

  glCullFace ( GL_BACK );

  glPolygonMode ( GL_FRONT, GL_FILL );

  glPolygonMode ( GL_BACK, GL_LINE );

  setBackgroundColor(Qt::white);

  glMatrixMode(GL_MODELVIEW);

  glLoadIdentity();

  for(int i = 0 ; i < m_rens.size(); ++i)
    m_rens[i]->gl_init();
}

void glviewer_t::animate()
{
  for(int i = 0 ; i < m_rens.size(); ++i)
    if (m_rens[i]->isAnimation())
      m_rens[i]->animate(animationPeriod());
}

glviewer_t::glviewer_t(QWidget * par):
  m_is_recording(false),
  m_bf_cull(true),
  m_wireframe(false)
{
  setParent(par);
}

glviewer_t::~glviewer_t()
{
}

void glviewer_t::add_ren(renderable_ptr_t ren)
{
  if (boost::range::find(m_rens,ren) == m_rens.end())
    m_rens.push_back(ren);
}

void glviewer_t::del_ren(int i)
{
  if(i > 0 && i < m_rens.size())
    m_rens.erase(m_rens.begin()+i);
}

void glviewer_t::keyPressEvent(QKeyEvent *e)
{
  const Qt::KeyboardModifiers modifiers = e->modifiers();

  if ((e->key()==Qt::Key_C) && (modifiers==Qt::ControlModifier))
  {
    m_is_recording = !m_is_recording;

    if(m_is_recording)
      connect(this, SIGNAL(drawFinished(bool)),this, SLOT(saveSnapshot(bool)));
    else
      disconnect(this, SIGNAL(drawFinished(bool)),this, SLOT(saveSnapshot(bool)));
  }
  else if ((e->key()==Qt::Key_B) && (modifiers==Qt::ControlModifier))
  {
    m_bf_cull = !m_bf_cull;

    if(m_bf_cull)
      glEnable ( GL_CULL_FACE );
    else
      glDisable ( GL_CULL_FACE );
  }
  else if ((e->key()==Qt::Key_W) && (modifiers==Qt::ControlModifier))
  {
    m_wireframe = !m_wireframe;

    if(m_wireframe)
      glPolygonMode ( GL_FRONT_AND_BACK, GL_LINE );
    else
    {
      glPolygonMode ( GL_FRONT, GL_FILL );
      glPolygonMode ( GL_BACK, GL_LINE );

    }
  }
  else
  {
    int k = e->key();
    int m = 0;
    bool handled = false;

    for(int i = 0 ; i < m_rens.size(); ++i)
    {
      handled = m_rens[i]->processKey(k,m);
      if(handled)
        break;
    }

    if (!handled)
      QGLViewer::keyPressEvent(e);
  }

  updateGL();
}

QString glviewer_t::helpString() const
{
  QString text("<h2>Viewer</h2>");
  return text;
}

}


//***************************************************************************//


namespace glutils {

bool glviewer_t::setLightEnabled(int ind, bool v){
  QVariantMap map;
  map["Enabled"] = v;
  setLightParams(ind,map);
}

bool glviewer_t::setLightPosition(int ind,double x,double y,double z,double w){
  QVariantMap map;
  map["Position"] = QVector4D(x,y,z,w);
  setLightParams(ind,map);
}

}


//***************************************************************************//


namespace glutils {



void viewer_mainwindow::save_snapshot(QString str, int ms)
{
  glviewer->updateGL();

  glviewer->saveImageSnapshot(str,ms);
}

void viewer_mainwindow::on_actionEval_Script_triggered(bool)
{
  QString fname = QFileDialog::getOpenFileName
      (this,tr("Select script file"),
       QDir::currentPath(),"python (*.py)");

  if(fname == "")
    return;

  eval_script(fname);

  pyconsole->appendCommandPrompt();
}

void viewer_mainwindow::eval_script(QString str)
{
  m_pqt.evalFile(str);
}


viewer_mainwindow::viewer_mainwindow()
{
  setupUi (this);

  PythonQt::init();

  m_pqt = PythonQt::self()->getMainModule();

  m_pqt.addObject("app", this);
  m_pqt.addObject("viewer", glviewer);

  pyconsole->setContext(m_pqt);

}

viewer_mainwindow::~viewer_mainwindow()
{
}

inline QColor to_qcolor (const glutils::color_t & c)
{
  return QColor::fromRgbF(c[0],c[1],c[2]);
}

}


